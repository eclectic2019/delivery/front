/* global window,document */
import React, { Component } from 'react';
import MapGL,  {Popup, Marker} from 'react-map-gl';
import turfBbox from '@turf/bbox';
// import { featureCollection } from '@turf/helpers';
import AutoSizer from 'react-virtualized/dist/commonjs/AutoSizer';
import 'mapbox-gl/dist/mapbox-gl.css';
import DeckGLOverlay from './deckgl-overlay.js';

// Set your mapbox token here
// const MAPBOX_TOKEN = process.env.MapboxAccessToken; // eslint-disable-line
const MAPBOX_TOKEN =
  'pk.eyJ1IjoiaWxpamF6IiwiYSI6ImNqZ3hlamd0eTBlNnYycW9idXFlZ2FqencifQ.ItZjDaM026ZsfIL505lXSQ';

// const colorScale = r => [r * 255, 140, 200 * (1 - r)];

// const getBbox = (...features) => turfBbox(featureCollection(...features));

export default class Map extends Component {
  constructor(props) {
    super(props);
    this.drawVP = false;
    this.bbox = props.bbox;
    this.state = {
      viewport: {
        zoom: 5,
        maxZoom: 24,
        pitch: 45,
        bearing: 0,
        width: 500,
        height: 500,        
        showPopup: true,   
        latitude: 52.610220,
        longitude: 39.594719,   
      },
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.resize.bind(this));
    this.resize();
  }

  onViewportChange(viewport) {
    this.setState({
      viewport: { ...this.state.viewport, ...viewport },
    });
  }

  resize() {
    this.onViewportChange({
      // width: window.innerWidth - 400,
      // height: window.innerHeight,
    });
  }

  render() {
    const { viewport, showPopup /* , testdata */ } = this.state;
    const { data, catalog } = this.props;

    const rasterStyle = {
      version: 8,
      sources: {
        'raster-tiles': {
          type: 'raster',
          tiles: ['https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}'],
          tileSize: 256,
        },
      },
      layers: [
        {
          id: 'simple-tiles',
          type: 'raster',
          source: 'raster-tiles',
          minzoom: 0,
          maxzoom: 22,
        },
      ],
    };

   /*  if (!data.layers) return null;
    if (!this.bbox) {
      if (this.props.bbox) {
        viewport.longitude = (this.props.bbox[0] + this.props.bbox[2]) / 2;
        viewport.latitude = (this.props.bbox[1] + this.props.bbox[3]) / 2;
        this.bbox = this.props.bbox;
      }
    } else if (!this.bbox.reduce((r, c, i) => r && c === this.props.bbox[i], true)) {
      viewport.longitude = (this.props.bbox[0] + this.props.bbox[2]) / 2;
      viewport.latitude = (this.props.bbox[1] + this.props.bbox[3]) / 2;
      this.bbox = this.props.bbox;
    }

    if (!this.drawVP) {
      // const bbox = getBbox(Object.keys(data.layers).map( id => data.layers[id]));
      if (data.layers.workZone) {
        const bbox = turfBbox(data.layers.workZone);
        viewport.longitude = (bbox[0] + bbox[2]) / 2;
        viewport.latitude = (bbox[1] + bbox[3]) / 2;
      } else {
        viewport.longitude = 41.466816;
        viewport.latitude = 52.876654;
      }
      this.drawVP = true;
    } */

    return (
      <AutoSizer>
        {({ height, width }) => (
          <MapGL
            {...viewport}
            width={width}
            height={height}
            mapStyle={rasterStyle}
            onViewportChange={this.onViewportChange.bind(this)}
            mapboxApiAccessToken={MAPBOX_TOKEN}  >       
            
          
                <Marker latitude={52.7236} longitude={41.4423} offsetLeft={-20} offsetTop={-10}>
                <div style={{  backgroundImage: 'url(\'/pin.svg\')',
                    backgroundSize: 'cover',
                    width: '20px',
                    height: '35px',
                    borderRadius: '50%',
                    cursor: 'pointer',}}></div>
              </Marker>
              <Marker latitude={52.610220} longitude={39.594719} offsetLeft={-20} offsetTop={-10}>
                <div style={{  backgroundImage: 'url(\'/pin.svg\')',
                    backgroundSize: 'cover',
                    width: '20px',
                    height: '35px',
                    borderRadius: '50%',
                    cursor: 'pointer',}}></div>
              </Marker>
              <Marker latitude={55.7558} longitude={37.6173} offsetLeft={-20} offsetTop={-10}>
                <div style={{  backgroundImage: 'url(\'/pin.svg\')',
                    backgroundSize: 'cover',
                    width: '20px',
                    height: '35px',
                    borderRadius: '50%',
                    cursor: 'pointer',}}></div>
              </Marker>
            
            
          </MapGL>
        )}
      </AutoSizer>
    );
  }
}
