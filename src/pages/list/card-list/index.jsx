import { Button, Card, Icon, List, Statistic, Typography, Row, Col, Descriptions, Timeline, } from 'antd';
import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import styles from './style.less';
import numeral from 'numeral';
import moment from 'moment';
import router from 'umi/router';

const { Title, Paragraph } = Typography;
const { Countdown } = Statistic;


@connect(({ listCardList, loading }) => ({
  listCardList,
  loading: loading.models.list,
}))
class CardList extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'listCardList/fetch1',
      payload: {
        count: 8,
      },
    });
  }

  render() {
    const {
      listCardList: { list },
      loading,
    } = this.props;
    const renderPath = (path) =>  
     (path && <Timeline onClick={() => router.push('/list/map')}>
        {path.map(renderPoint)}
    </Timeline>)
    const renderPoint = ( p ) => (p && <Timeline.Item className="point" style={{minHeight: 'none'}}>{`${p.name} (${p.lon},${p.lat})`}</Timeline.Item>)
    const content = (
      <div className={styles.pageHeaderContent}>
      </div>
    );
    const extraContent = (
      <div className={styles.extraImg}>
        <img
          alt="这是一个标题"
          src="https://gw.alipayobjects.com/zos/rmsportal/RzwpdLnhmvDJToTdfDPe.png"
        />
      </div>
    );
    const nullData = {};
    const handleOk = (id) => () => {
      const { dispatch } = this.props;
      if (dispatch) {
        dispatch({
          type: 'listCardList/ok',
          id
        });
      }console.log('Ok', id)};
    const handleCancel = (id) => () => {
      const { dispatch } = this.props;
      if (dispatch) {
        dispatch({
          type: 'listCardList/cancel',
          id
        });
      }
      console.log('Cancel', id, dispatch)
    };
    return (
      <PageHeaderWrapper>
        <div className={styles.cardList}>
          <List
            rowKey="id"
            loading={loading}
            grid={{
              gutter: 24,
              lg: 3,
              md: 2,
              sm: 1,
              xs: 1,
            }}
            dataSource={list}
            renderItem={item => {
              if (item && item.id) {
                return (
                  <List.Item key={item.id}>
                    <Card
                      hoverable                      
                      className={styles.card}
                      title={`Заказ №${item.id}`}
                      actions={[<a key="option1" onClick={handleOk(item.id)}>Принять</a>, <a key="option2" onClick={handleCancel(item.id)}>Отказаться</a>]}
                    >
                      {renderPath(item.route)}
                      <Title level={4} className="h4" style={{fontWeight: '300px'}}>
                        <Icon type="gateway"/>
                        {` 400 км`}
                      </Title>
                      <Title level={4} className="h4" style={{fontWeight: '300px'}}>
                        <Icon type="calendar"/>
                        {` ${moment(item.dt_start).format('DD-MM-YYYY')} + ${moment(item.dt_end).diff(item.dt_start, 'days')} дней`}
                      </Title>

                      <Row>
                        <Col span={12}>
                          <Statistic
                            title="Цена"
                            suffix="₽"
                            value={numeral(item.price_min).format('0,0')}
                          />
                        </Col>
                        <Col span={12}>
                          <Statistic
                            title="Маржа"
                            suffix="₽"
                            value={numeral(item.price_min).format('0,0')}
                          />
                        </Col>
                      </Row>
                    </Card>
                  </List.Item>
                );
              }

              return (
                <List.Item>
                  <Button type="dashed" className={styles.newButton}>
                    <Icon type="plus" /> 新增产品
                  </Button>
                </List.Item>
              );
            }}
          />
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default CardList;
