import { queryFakeList } from './service';
import { routerRedux } from 'dva/router';

const Model = {
  namespace: 'listCardList',
  state: {
    list: [{
      id: 12349,
      route: [{
        id: 34324,
        name: "Тамбов",
        lon: 52.7236,
        lat: 41.4423,
          },{
            id: 34324,
            name: "Липецк",
            lon: 52.610220,
            lat: 39.594719,
        },{
          id: 34324,
          name: "Москва",
          lon: 55.7558,
          lat: 37.6173,
        }],

      dt_start: new Date(),
      dt_end: new Date('2019-08-10'),
      price_min: 123123,
      price_max: 332322,
    }, 
    {
      id: 12390,
      route: [{
          id: 34324,
          name: "sdfsjkfsd",
          lon: 54.2,
          lat: 46.7,
      }],
      dt_start: new Date(),
      dt_end: new Date('2019-08-10'),
      price_min: 123123,
      price_max: 332322,
    },
    {
      id: 12391,
      route: [{
          id: 34324,
          name: "sdfsjkfsd",
          lon: 54.2,
          lat: 46.7,
      }],
      dt_start: new Date(),
      dt_end: new Date('2019-08-10'),
      price_min: 123123,
      price_max: 332322,
    },
    {
      id: 12393,
      route: [{
          id: 34324,
          name: "sdfsjkfsd",
          lon: 54.2,
          lat: 46.7,
      }],
      dt_start: new Date(),
      dt_end: new Date('2019-08-10'),
      price_min: 123123,
      price_max: 332322,
    },
    {
      id: 12396,
      route: [{
          id: 34324,
          name: "sdfsjkfsd",
          lon: 54.2,
          lat: 46.7,
      }],
      dt_start: new Date(),
      dt_end: new Date('2019-08-10'),
      price_min: 123123,
      price_max: 332322,
    }],
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryFakeList, payload);
      yield put({
        type: 'queryList',
        payload: Array.isArray(response) ? response : [],
      });
    },
    *cancel({ id }, { call, put }) {
      console.log('put');
      yield put({
        type: 'cancelList',
        id,
      });
    },
    *ok({ id }, { call, put }) {
      yield put(
        routerRedux.push('/result/success')
      );
      yield put({
        type: 'cancelList',
        id,
      });
    },
  },
  reducers: {
    queryList(state, action) {
      return { ...state, list: action.payload };
    },
    cancelList(state, action) {
      return { ...state, list: state.list.filter( c => c.id !== action.id ) };
    },
  },
};
export default Model;
