import { Button, Card, Icon, List, Statistic, Typography, Row, Col, Descriptions, Timeline} from 'antd';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { FormattedMessage, formatMessage } from 'umi-plugin-react/locale';
import Map from '@/components/Map';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import styles from './style.less';
import numeral from 'numeral';
import moment from 'moment';
import router from 'umi/router';


const { Title, Paragraph } = Typography;
const { Countdown } = Statistic;


@connect(({ listCardList, loading }) => ({
  listCardList,
  loading: loading.models.list,
}))
class MapModule extends Component {
  state = { rect: { x: 0}}
  componentDidMount() {
    const { dispatch } = this.props;
    var rect = ReactDOM.findDOMNode(this)
        .getBoundingClientRect()
    this.setState({rect});
    //console.log(rect);
/*     dispatch({
      type: 'listCardList/fetch1',
      payload: {
        count: 8,
      },
    }); */
  }

  render() {
    const {
      listCardList: { list },
      loading,
    } = this.props;
    const {rect} = this.state;

    return (
      <PageHeaderWrapper> 
        <Row>
          <Col span={18}>
            <Card  bordered={false} bodyStyle={{ height: window.innerHeight - rect.x + 20}}>             
                  <Descriptions title="Карта">
                  </Descriptions>

                    <div style={{ height: '90%' }}>
                      <Map />
                    </div>     
            </Card>
          </Col>
          <Col span={6}>
            <Card  bordered={false} bodyStyle={{ height: window.innerHeight  - rect.x + 20}}>             
                  <Descriptions title="Информация о заказе" border column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}>
                    <Descriptions.Item label="Дата">26-07-2019 + 3 дня</Descriptions.Item>
                    <Descriptions.Item label="Вес">18 тонн</Descriptions.Item>
                    <Descriptions.Item label="Цена">25 403 руб</Descriptions.Item>
                    <Descriptions.Item label="Описание">
                     Необходимо доставить одного большого желтого слона из Нового Уренгоя в Мурманск
                    </Descriptions.Item>
                  </Descriptions> 
                  <Button type="primary" onClick={() => router.goBack()}>
                    <FormattedMessage id="result-success.success.btn-return" defaultMessage="Назад" />
                  </Button>
            </Card>
          </Col>
        </Row>
      </PageHeaderWrapper>
    );
  }
}

export default MapModule;




