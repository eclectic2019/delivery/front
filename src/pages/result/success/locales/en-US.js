export default {
  'result-success.success.title': 'Заявка принята',
  'result-success.success.description':
    'Вы приняли заявку клиента, ниже вы можете найти контактные данные и договориться о заборке груза',
  'result-success.success.operate-title': 'Project Name',
  'result-success.success.operate-id': 'Project ID',
  'result-success.success.principal': 'Principal',
  'result-success.success.operate-time': 'Effective time',
  'result-success.success.step1-title': 'Create project',
  'result-success.success.step1-operator': 'Qu Lili',
  'result-success.success.step2-title': 'Departmental preliminary review',
  'result-success.success.step2-operator': 'Zhou Maomao',
  'result-success.success.step2-extra': 'Urge',
  'result-success.success.step3-title': 'Financial review',
  'result-success.success.step4-title': 'Finish',
  'result-success.success.btn-return': 'Назад',
  'result-success.success.btn-project': 'View Project',
  'result-success.success.btn-print': 'Печать',
};
